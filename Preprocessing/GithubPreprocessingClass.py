from pydriller import RepositoryMining, GitRepository
#from DAL.TransactionDAL import TransactionDAL
#from DAL.ComponentDAL import ComponentDAL
from DataModel.TransactionModel import TransactionModel
from DataModel.ComponentModel import ComponentModel
from Helper.Logger import Logger
from Helper.Configuration import Configuration
import datetime
import sys, os
import uuid
import pandas as pd

class GithubPreprocessingClass:       
        previousTransactionModel = None
        transactionId = 0
        changesLines = []
        def __init__(self):
                self.timeStart = datetime.datetime.now()
                
                Logger.LogInfo('Initiate preprocessing' + str(self.timeStart))

                self.repoConfig = Configuration.ReadConfig("Repository")
                self.url = self.repoConfig['Address']
                self.appName = self.repoConfig['AppName']

        def preprocessing(self):
                try:
                                                
                        Logger.LogInfo('Get github metadata ' + self.url)

                        self.gr = GitRepository(self.url)

                        self.previousTransactionModel = None
                        
                        for commit in RepositoryMining(self.url, only_no_merge=True).traverse_commits():
                                try:
                                        self.transactionId = 0
                                        self.checkCommit(commit)

                                except Exception as ex:
                                        errorMessage = str(ex)
                                        Logger.LogError(errorMessage)

                                
                                if (len(self.changesLines) > 0):   
                                        self.saveTransactions()
                                        #save changes to csv
                                self.timeFinish = datetime.datetime.now()
                                timeDiff = self.timeFinish - self.timeStart
                                Logger.LogInfo('Preprocessing finished in ' + str(timeDiff.seconds) + ' seconds')
                        
                except Exception as ex:
                        errorMessage = str(ex)                        
                        Logger.LogError(errorMessage)
        
        def checkCommit(self, commit):
                Logger.LogInfo('checkCommit')
                #if commit more than 30 files continue
                if (len(commit.modifications) > 30): return
                
                ignoredExtension = ("md", "min.js")
                for modification in commit.modifications: 
                        if (modification.filename.endswith(ignoredExtension)):continue
                        self.checkModification(modification, commit.author_date)

        def checkModification(self, modification, commitDate):
                Logger.LogInfo('checkModification')
                if len(modification.methods) == 0: return

                parsedDiff = self.gr.parse_diff(modification.diff)
                updatedLineNumbers = self.getUpdatedLines(parsedDiff['added'], parsedDiff['deleted'])
                
                for method in modification.methods:
                        if (self.isFunctionChanged(method, updatedLineNumbers) == False): continue
                                                        
                        if (self.transactionId == 0):
                                self.transactionId = str(uuid.uuid4()) #self.saveTransaction(commitDate)

                        Logger.LogInfo('function changed')                     
                        self.changesLines.append(self.constructChangesLines(self.transactionId, method, modification))

        def saveTransactions(self):
                df = pd.DataFrame(self.changesLines, columns=['transactionId', 'method', 'class', 'file'])
                df.to_csv(self.appName + '.csv', index=False)
                
        # def saveTransaction(self, commitDate):
        #         Logger.LogInfo('saveTransaction')             
        #         transacDal = TransactionDAL()
                
        #         transModel = TransactionModel()
        #         transModel.date = commitDate                  
        #         self.previousTransactionModel = transacDal.Insert(transModel)
        #         return self.previousTransactionModel.transactionId

        def constructChangesLines(self, transactionId, method, modification):
                className = modification.filename

                classArr = method.name.split('::')
                if (len(classArr) > 1):
                        className = classArr[(len(classArr) - 2)]

                return [transactionId, method.name, className, modification.filename]

        # def saveChanges(self, transactionId, method, modification):
        #         Logger.LogInfo('saveChanges')
        #         componentDAL = ComponentDAL()
        #         componentModel = ComponentModel()
                
        #         componentModel.transactionId = transactionId
        #         componentModel.method = method.name

        #         className = modification.filename

        #         classArr = method.name.split('::')
        #         if (len(classArr) > 1):
        #                 className = classArr[(len(classArr) - 2)]

        #         componentModel.entity = className
        #         componentModel.file = modification.filename

        #         componentDAL.Insert(componentModel)

        def getUpdatedLines(self, addedLines, deletedLines):
                Logger.LogInfo('getUpdatedLines')
                updatedLines = []
                try:
                        for addedLine in addedLines: updatedLines.append(addedLine[0])
                        
                        for deletedLine in deletedLines:
                                if (deletedLine[0] in updatedLines): continue
                                updatedLines.append(deletedLine[0])
                        
                        updatedLines.sort()
                except Exception as ex:
                        Logger.LogError(str(ex))

                return updatedLines

        def isFunctionChanged(self, method, updatedLineNumbers):                          
                return len([x for x in updatedLineNumbers if x >= method.__dict__['start_line'] and x <= method.__dict__['end_line']]) > 0
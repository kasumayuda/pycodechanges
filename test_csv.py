import pandas as panda
import csv
from DAL.CSVTransactionDAL import CSVTransactionDAL

def readCSV():
    csvFile = "Kruskal.csv"
    data = panda.read_csv(csvFile)
    data.drop_duplicates(subset="transactionId", keep=False, inplace=False)
    for transaction in data.transactionId.drop_duplicates():
        components = data.loc[data['transactionId'] == transaction]        
        for x in range(0, len(components)):
            print('transactionId: ' + components.iloc[x]['transactionId'])
            print('method: ' + components.iloc[x]['method'])
            print('class: ' + components.iloc[x]['class'])
            print('file: ' + components.iloc[x]['file'])
        print("===============")

#readCSV()

def writeToCSV():
    
    data = []
    data.append(['Raphael', 'red', 'sai'])
    data.append(['Donatello', 'purple', 'bo staff'])    
    columns = ['name', 'mask', 'weapon']
    df = panda.DataFrame(data, columns = columns)
    print(df.to_csv(index=False))

#writeToCSV()

def csvTransactionTestDAL():
    csvTransactionDAL = CSVTransactionDAL()    
    transactions = csvTransactionDAL.GetAll("Kruskal.csv")
    for transaction in transactions:
        print(transaction.transactionId)
        for component in transaction.components:
            print(component.method)
            print(component.entity)
            print(component.file)
        print("=================================")

csvTransactionTestDAL()
from DataModel.RuleModel import RuleModel
from DAL.TransactionDAL import TransactionDAL
from Helper.TarmaqLogger import TarmaqLogger
import datetime
import sys, os
import uuid

class Miners:
    limit = 70    
    tarmaqResult = []
    averageAPs = []
    averageTime = []
    changesMin = 2
    changesMax = 10

    def __init__(self):
        pass
    
    def execute(self, allTransactions):
        self.tarmaqResults = ["tarmaqId, time, query, expected outcome, average precision, query(%)"]
        self.tarmaqResults = []
        self.averageAPs = []
        self.averageTime = []

        self.timeStart = datetime.datetime.now()
        TarmaqLogger.LogMiningInfo("Start tarmaq" + str(self.timeStart))

        history = allTransactions[:round((self.limit / 100) * len(allTransactions))]
        queryTransactions = self.createQuery(allTransactions, history)
        counter = 0
        
        for x in range(self.changesMin,self.changesMax):
            for qTransaction in queryTransactions[x]:
                for index in range(1, len(qTransaction)):
                    self.tarmaq(history, qTransaction[:index], qTransaction[index:])
                    counter += 1
                
        self.timeFinish = datetime.datetime.now()
        timeDiff = self.timeFinish - self.timeStart
        TarmaqLogger.LogMiningInfo('Tarmaq executed %d times and finished in %d seconds with %d limit' % (counter, timeDiff.seconds, self.limit))
        TarmaqLogger.LogMiningInfo('average APs %d %d' % (sum(self.averageAPs), len(self.averageAPs)))
        TarmaqLogger.LogMiningInfo('average APs %s' % (str(round(sum(self.averageAPs)/len(self.averageAPs),2))))
        TarmaqLogger.LogMiningInfo('average Time %d' %(sum(self.averageTime) / len(self.averageTime)))

        TarmaqLogger.LogMiningInfo("\n".join(self.tarmaqResults))

    def createQuery(self, allTransactions, history):        
        queryCandidates = set(allTransactions) - set(history)
        queryCandidates = sorted(queryCandidates, key = lambda x: x.transactionId)        
        allQueryCandidatesFinals = {}
        for queryLimit in range(self.changesMin,self.changesMax):
                components = []
                for queryCandidate in list(queryCandidates):
                        if (len(queryCandidate.components) == queryLimit and len(components) < 4): 
                            components.append(queryCandidate.components)

                allQueryCandidatesFinals[queryLimit] = components
        return allQueryCandidatesFinals
    
    def tarmaq(self, transactions, query, expectedOutcome):
        tarmaqId = str(uuid.uuid4())
        queryData = tarmaqId + "\n Query: \n" + self.extract(query) + "\n Expected Outcome: \n" + self.extract((expectedOutcome))

        TarmaqLogger.LogMiningInfo(queryData)
        print(queryData)

        tqStartTime = datetime.datetime.now()

        filteredHistories = self.getFilteredHistories(transactions, query)
        
        if (len(filteredHistories) == 0): 
            self.tarmaqResults.append("%s, %d, %d, NO HISTORY"%(tarmaqId, len(query), 
            len(expectedOutcome)))
            return

        rules = self.createRule(filteredHistories, query)
        result = self.sortRules(rules)
                
        score = self.evaluate(result, expectedOutcome)

        tqFinishTime = datetime.datetime.now()
        tqTimeDiff = tqFinishTime - tqStartTime

        if not (score is None): 
            self.averageAPs.append(score)
            self.averageTime.append(tqTimeDiff.total_seconds() * 1000)

        recommendations = "Recommendation: \n" + self.extractRule(result) + " \n AP score: " + str(score) + " \n Time: " + str(tqTimeDiff.total_seconds() * 1000)

        TarmaqLogger.LogMiningInfo(recommendations)
        print(recommendations)

        self.tarmaqResults.append("%s, %d, %d, %d, %s, %s"%(tarmaqId, tqTimeDiff.total_seconds() * 1000, 
        len(query), len(expectedOutcome), str(score), str(round((len(query)/(len(query) + len(expectedOutcome)))*100, 2))))

        return result
     
    def getFilteredHistories(self, transactions, query):
        filteredHistories = []
        k = 1
        for transaction in transactions:
            # get interaction of transaction and query
            intersection = self.getIntersection(transaction.components, query)
            # check if intersection size is equal to current largest intersection size
            if (len(intersection) == k):
                filteredHistories.append(transaction)
            elif (len(intersection) > k):
                # there is a new largest querysubset
                k = len(intersection)
                # reset the history
                filteredHistories=[]
                filteredHistories.append(transaction)

        return filteredHistories

    def getIntersection(self, components, query):        
        temp = set(query) 
        return [value for value in components if value in temp]

    def createRule(self, filteredHistories, query):
        if (len(filteredHistories) == 0 ): return
        # rule creation
        rules = []
        antecedents = {}

        for filteredHistory in filteredHistories:
            antecedent = sorted(self.getIntersection(filteredHistory.components, query), key = lambda x:( x.method, x.entity, x.file ))
            consequents = set(filteredHistory.components) - set(antecedent)
            
            antecedentStr = str(antecedent)
            if (antecedentStr not in antecedents): antecedents[antecedentStr] = 0
            
            antecedents[antecedentStr] += 1
            #if (len(consequents) == 0): continue
            for consequent in list(consequents):
                ruleName =  antecedentStr+ " -> " + str([consequent])
                rule = self.getRuleByName(ruleName, rules)
                if (rule is None):
                    newRule = RuleModel(antecedent, [consequent])
                    newRule.support = round(1/len(filteredHistories),2)
                    rules.append(newRule)
                else:
                    #update support
                    rule.support = round(rule.support + (1/len(filteredHistories)),2)

            rulesByAntecedent = self.getRuleByAntecedent(rules, antecedent)
            for rule in rulesByAntecedent:
                newConfidence = rule.support/(antecedents[antecedentStr]/len(filteredHistories))
                rule.confidence = round(newConfidence,2)
        
        return rules
            
    def getRuleByAntecedent(self, rules, antecedent):
        rulesByAntecedent = []
        for rule in rules:
            if (str(rule.antecedent) == str(antecedent)):
                rulesByAntecedent.append(rule)
    
        return rulesByAntecedent

    def getRuleByName(self, ruleName, rules):
        if (len(rules) == 0): return None
        for rule in rules:
            if (rule is None): continue
            if (rule.name == ruleName):
                return rule
    
        return None
    
    def sortRules(self, rules):
        if (rules is None): return None
        if (len(rules) == 0): return None
        sortedRules = sorted(rules, key = lambda y:(y.support, y.confidence),reverse= True)

        return sortedRules

    def evaluate(self, rules, expectedOutcome):
        if (len(expectedOutcome) == 0 or rules is None): return None
        
        averagePrecision = 0
        correctItems = []
        totalItemsConsidered = []
        #for rule in rules[:10]:
        for rule in rules:
            items = rule.consequent
            newItems = set(items) - set(totalItemsConsidered)
            if len(newItems) == 0: continue
            for newItem in newItems: totalItemsConsidered.append(newItem)
            correctInRule = self.getIntersection(items, expectedOutcome)
            if (len(correctInRule) == 0):continue
            # make sure that the new items havent already been added earlier
            newCorrects = set(correctInRule) - set(correctItems)
            ## add new items
            for newCorrect in newCorrects: correctItems.append(newCorrect)            
            changeInRecall = len(newCorrects)/len(expectedOutcome)            
            precisionAtK = len(correctItems)/len(totalItemsConsidered)
            
            averagePrecision += (precisionAtK * changeInRecall)
            
        return averagePrecision

    def extract(self, components):
        componentStr = []
        for component in components:
            componentStr.append(str(component))

        return "\n".join(componentStr)

    def extractRule(self, rules):
        if (rules is None): return ""
        ruleStr = []
        counter = 0
        #ruleStr.append("=========================PRIORITY RECOMMENDATION=========================================")
        for rule in rules:
            ruleStr.append("%s, %s, %s )"%(str(rule.consequent), str(rule.confidence), str(rule.support)))
            counter+=1
            #if (counter == 10):
                #ruleStr.append("=========================PRIORITY LIMIT=========================================")

        return "\n".join(ruleStr)
        
    def validateComponent(self, histories, components):
        for index in range(1, len(components)):

            queryComponents = [value for value in set(components[:index])]
            outcomeComponents = [value for value in set(components[index:])]
            
            for history in histories:
                intersection = self.getIntersection(history.components, query)
                #historyComponents = [value.components for value in history]
                for historyComponent in history.components:

                    queryIntersection = [value for value in queryComponents if value in history.components]
                    outcomeIntersection = [value for value in outcomeComponents if value in history.components]

                    if ((queryIntersection is None == False) and (outcomeIntersection is None == False)): return True
            #     historyComponentIds = [value.componentId for value in history.components]
                
            #     queryIntersection = [value for value in queryComponentIds if value in historyComponentIds]
            #     outcomeIntersection = [value for value in expectedOutcomeComponentIds if value in historyComponentIds]

            #     if ((queryIntersection is None == False) and (outcomeIntersection is None == False)): return True

        return False

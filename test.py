from DAL.TransactionDAL import TransactionDAL
from DataModel.TransactionModel import TransactionModel
from DataModel.ComponentModel import ComponentModel
from DataModel.RuleModel import RuleModel
from DAL.ChangesTypeDAL import ChangesTypeDAL
from Miners.Miners import Miners
import datetime

transactionDAL = TransactionDAL()
allTransactions = transactionDAL.GetAllNoComponent()
dataLength = len(allTransactions)
transactionLimit = round((90 / 100) * dataLength)
print('limit : %d' % (transactionLimit))
transactions = allTransactions[:transactionLimit]
query = set(allTransactions) - set(transactions)

print("all transactions : %d", len(allTransactions))
print("transactions : %d", len(transactions))
print("query : %d", len(query))

def GetIntersection(components, query): 
  
    # Use of hybrid method 
    temp = set(query) 
    return [value for value in components if value in temp] 
    

def GetFilteredHistories(transactions, query):
    filteredHistories = []
    k = 0
    for transaction in transactions:
        # get interaction of transaction and query
        intersection = GetIntersection(transaction.components, query)
        # check if intersection size is equal to current largest intersection size
        if (len(intersection) == k):
            filteredHistories.append(transaction)
        elif (len(intersection) > k):
            # there is a new largest querysubset
            k = len(intersection)
            # reset the history
            filteredHistories=[]
            filteredHistories.append(transaction)

    return filteredHistories

def TestChange(transactionModel):
    transactionModel.transactionId = 5

# transacDal = TransactionDAL()
# transactions = transacDal.GetAll()
# if (transactions != 0):
#     for transaction in transactions:
#         print(transaction.transactionId)
#         for component in transaction.components:
#             print(component.method)
# transModel = TransactionModel()
# #transModel.transactionId = 0
# transModel.date = datetime.datetime.now()
# print(transacDal.Insert(transModel).transactionId)
# print('didi')
# print(transModel.transactionId)

#print(transacDal.GetById(11))

# from DAL.DatabaseDAL import DatabaseDAL

# dbDAL = DatabaseDAL()

#from Helper.Logger import Logger

#Logger.LogDebug('ss')

#testing fetching transaction + commits

# test intersection



# intersec = GetFilteredHistories(transactions, queries)
# print(len(intersec))

arrA = [2,3,4,5]
arrB = [4]
print(set(arrA) - set(arrB))
# for transaction in transactions:
#     intersec = GetFilteredHistories(transactions, queries)
#     print(len(intersec))

def getTransactionComponent(transactionComponents, queryMethod):
    for transactionComponent in transactionComponents:
        if (transactionComponent.method == queryMethod):
            return transactionComponent
    
    return None

component = getTransactionComponent(transactionComponents, "Method A_1")
# if (component == None):
#     print("nope")
if (component is None):
    print("nope")    
else:
    print(component.method)
    print(component.transactionId)
    print("ok")

# intersec = (set(transModelsListA).intersection(transModelsListB))


#intersec = get(transModelsListA, transModelsListB)

# for item in intersec:
#     print(item.transactionId)
#     print(item.method)
# # lst1 = [9, 9, 74, 21, 45, 11, 63] 
# lst2 = [4, 9, 1, 17, 11, 26, 28, 28, 26, 66, 91] 
# print(len(lst2))
# lst2 = []
# print(len(lst2))
# # print(intersection(lst1, lst2)) 


# changes = ChangesTypeDAL().GetAll()
# for x in changes:
#     print(x.change)
#     print(x.changeTypeId)

# an = next((x for x in changes if x.change == "ADD"), None)
# print(an.change)
# # for x in an:
# #     print(x.__dict__)
# # {'kids': 0, 'name': 'Dog', 'color': 'Spotted', 'age': 10, 'legs': 2, 'smell': 'Alot'}
# # now dump this in some way or another
# print( ', '.join("%s: %s" % item for item in attrs.items()))

# aa = (x for x in changes if x.change == "ADD")
# print(aa.changeTypeId)
class ChangeTypeModel:
    def __init__self(self):
        pass
    
    @property
    def changeTypeId(self):
        return self.__changeTypeId

    @changeTypeId.setter
    def changeTypeId(self, changeTypeId):
        self.__changeTypeId = changeTypeId

    @property
    def change(self):
        return self.__change

    @change.setter
    def change(self, change):
        self.__change = change
class TransactionModel:
    def __init__self(self):
        pass
    
    @property
    def transactionId(self):
        return self.__transactionId

    @transactionId.setter
    def transactionId(self, transactionId):
        self.__transactionId = transactionId

    @property
    def date(self):
        return self.__date

    @date.setter
    def date(self, date):
        self.__date = date

    @property
    def components(self):
        return self.__components

    # treat this a list
    @components.setter
    def components(self, components):
        self.__components = components

    #equitable
    def __hash__(self):
        return hash((self.transactionId))

    def __eq__(self, other):
        return self.transactionId, self.transactionId == other.transactionId,other.transactionId

    def __ne__(self, other):
        return not self.__eq__(other)
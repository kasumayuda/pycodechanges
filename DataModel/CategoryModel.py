class CategoryModel:
    def __init__self(self):
        pass
    
    @property
    def categoryId(self):
        return self.__categoryId

    @categoryId.setter
    def categoryId(self, categoryId):
        self.__categoryId = categoryId

    @property
    def category(self):
        return self.__category

    @category.setter
    def category(self, category):
        self.__category = category

    @property
    def order(self):
        return self.__order

    @order.setter
    def order(self, order):
        self.__order = order
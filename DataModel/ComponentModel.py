class ComponentModel:
    def __init__(self):
        pass

    @property
    def componentId(self):
        return self.__componentId

    @componentId.setter
    def componentId(self, componentId):
        self.__componentId = componentId
    
    @property
    def transactionId(self):
        return self.__transactionId

    @transactionId.setter
    def transactionId(self, transactionId):
        self.__transactionId = transactionId
        
    @property
    def method(self):
        return self.__method

    @method.setter
    def method(self, method):
        self.__method = method

    @property
    def entity(self):
        return self.__entity

    @entity.setter
    def entity(self, entity):
        self.__entity = entity

    @property
    def file(self):
        return self.__file

    @file.setter
    def file(self, file):
        self.__file = file
    
    @property
    def changeTypeId(self):
        return self.__changeTypeId

    @changeTypeId.setter
    def changeTypeId(self, changeTypeId):
        self.__changeTypeId = changeTypeId

    def __hash__(self):
        return hash((self.method, self.entity, self.file))

    def __eq__(self, other):
        return self.method, self.entity, self.file == other.method, other.entity, other.file

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "method: " + self.__method + ", class: " + self.__entity  + ", file: " + self.__file
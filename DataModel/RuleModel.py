class RuleModel:
    
    def __init__(self, antecedents, consequents):
        self.__antecedent = antecedents
        self.__consequent =  consequents
        self.__name = str(self.__antecedent) + " -> " + str(self.__consequent)

    @property
    def antecedent(self):
        return self.__antecedent

    @antecedent.setter
    def antecedent(self, antecedent):
        self.__antecedent = antecedent

    @property
    def consequent(self):
        return self.__consequent

    @consequent.setter
    def consequent(self, consequent):
        self.__consequent = consequent

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

    @property
    def support(self):
        return self.__support

    @support.setter
    def support(self, support):
        self.__support = support

    @property
    def confident(self):
        return self.__confident

    @confident.setter
    def confident(self, confident):
        self.__confident = confident
import json
class Configuration:

    ConfigFile = "config.json"

    @staticmethod
    def ReadConfig(key = ""):        
        with open(Configuration.ConfigFile) as f:
            config = json.load(f)
            if (key == ""):
                return config
            else:
                return config[key]
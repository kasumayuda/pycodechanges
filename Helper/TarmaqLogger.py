from Helper.Configuration import Configuration
import logging
import json

class TarmaqLogger:

    ConfigFile = "LogMiningINFO.json"

    @staticmethod
    def LogMiningInfo(logMessage):
        logInfoFile = TarmaqLogger._readLogFolder(TarmaqLogger.ConfigFile)
        logging.basicConfig(filename=logInfoFile, level=logging.INFO, format='%(levelname)s %(asctime)s %(message)s')
        logging.info(logMessage.encode("utf-8"))

    @staticmethod
    def _readLogFolder(logFileName):
        if (logFileName == ""): return ""
        return Configuration.ReadConfig("Logger")["Path"] + logFileName
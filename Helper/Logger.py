from Helper.Configuration import Configuration
import logging
import json

class Logger:
        
    @staticmethod
    def LogDebug(logMessage):
        logDEBUGFile = Logger._readLogFolder('LogINFO.log')
        logging.basicConfig(filename=logDEBUGFile, level=logging.DEBUG, format='%(levelname)s %(asctime)s %(message)s')
        logging.debug(logMessage)
    
    @staticmethod
    def LogError(logMessage):
        logErrorFile = Logger._readLogFolder('LogINFO.log')
        logging.basicConfig(filename=logErrorFile, level=logging.ERROR, format='%(levelname)s %(asctime)s %(message)s')
        logging.error(logMessage)

    @staticmethod
    def LogInfo(logMessage):
        logInfoFile = Logger._readLogFolder('LogINFO.log')
        logging.basicConfig(filename=logInfoFile, level=logging.INFO, format='%(levelname)s %(asctime)s %(message)s')
        logging.info(logMessage)

    @staticmethod
    def _readLogFolder(logFileName):
        if (logFileName == ""): return ""
        return Configuration.ReadConfig("Logger")["Path"] + logFileName
from Helper.Configuration import Configuration
from Preprocessing.GithubPreprocessingClass import GithubPreprocessingClass
from Miners.Miners import Miners
from Helper.TarmaqLogger import TarmaqLogger
from DAL.CSVTransactionDAL import CSVTransactionDAL

def preprocessing(app):
    Configuration.ConfigFile = "config."+ app + ".json"
    preprocessing = GithubPreprocessingClass()
    preprocessing.preprocessing()
    
def mining(app):
    Configuration.ConfigFile = "config."+ app + ".json"
    limits = [70]
    TarmaqLogger.ConfigFile = app+"-LogMiningInfo.log"
    
    print("Fetch " + app + " transaction")
    transactionDAL = CSVTransactionDAL()
    allTransactions = transactionDAL.GetAll(app+".csv")

    for limit in limits:
        print("execute " + app + " " + str(limit))
        miners = Miners()    
        miners.limit = limit
        miners.execute(allTransactions)
    print("Finish!")
    

    
#hangfire
#signalr
#spring
#preprocessing("hangfire")
#mining("signalr")
mining("hangfire")
#mining("spring")
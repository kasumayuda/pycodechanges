-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.14 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for thesis
DROP DATABASE IF EXISTS `Thesis`;
CREATE DATABASE IF NOT EXISTS `Thesis` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `thesis`;

-- Dumping structure for table thesis.jenisperubahan
DROP TABLE IF EXISTS `jenisperubahan`;
CREATE TABLE IF NOT EXISTS `jenisperubahan` (
  `idJenisPerubahan` int(11) NOT NULL AUTO_INCREMENT,
  `perubahan` varchar(15) NOT NULL,
  PRIMARY KEY (`idJenisPerubahan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.
-- Dumping structure for table thesis.komponen
DROP TABLE IF EXISTS `komponen`;
CREATE TABLE IF NOT EXISTS `komponen` (
  `idKomponen` int(11) NOT NULL AUTO_INCREMENT,
  `method` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `idJenisPerubahan` int(11) DEFAULT NULL,
  `idTransaksi` int(11) DEFAULT NULL,
  PRIMARY KEY (`idKomponen`),
  KEY `FK_komponen_jenisperubahan` (`idJenisPerubahan`),
  KEY `FK_komponen_transaksi` (`idTransaksi`),
  CONSTRAINT `FK_komponen_jenisperubahan` FOREIGN KEY (`idJenisPerubahan`) REFERENCES `jenisperubahan` (`idJenisPerubahan`),
  CONSTRAINT `FK_komponen_transaksi` FOREIGN KEY (`idTransaksi`) REFERENCES `transaksi` (`idTransaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.
-- Dumping structure for table thesis.transaksi
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE IF NOT EXISTS `transaksi` (
  `idTransaksi` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`idTransaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

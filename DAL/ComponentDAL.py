from DataModel.ComponentModel import ComponentModel
from .DatabaseDAL import DatabaseDAL
from Helper.Logger import Logger

class ComponentDAL:
    _tableName = "komponen"

    def __init__(self):
        self.dbDAL = DatabaseDAL()
                
    def Insert(self, componentModel):
        try:
            sql = "INSERT INTO " + self._tableName + " (method, kelas, file, idTransaksi) VALUES (%s, %s, %s, %s)"
            val = (componentModel.method, componentModel.entity, componentModel.file, componentModel.transactionId)
            componentModel.componentId = self.dbDAL.execute(sql, val)            
            return componentModel
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0

    def GetAll(self):
        try:
            sql = "SELECT * FROM "+ self._tableName
            return self._mapDataToModelList(self.dbDAL.fetch(sql))
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0

    def GetById(self, id):
        sql = "SELECT * FROM "+ self._tableName +" WHERE idKomponen =" + str(id)
        return self.dbDAL.fetch(sql)

    def GetByTransactionId(self, transactionId):
        sql = "SELECT * FROM "+ self._tableName +" WHERE idTransaksi =" + str(transactionId)
        return self._mapDataToModelList(self.dbDAL.fetch(sql))

    def _mapDataToModelList(self, dataList):
        if (dataList == None): return ComponentModel()
        
        componentModels = []
        for data in dataList:
            componentModels.append(self._mapDataToModel(data))

        return componentModels

    def _mapDataToModel(self, data):
        componentModel = ComponentModel()

        if (data == None): return componentModel

        componentModel.componentId = data['idKomponen']
        componentModel.method = data['method']
        componentModel.entity = data['kelas']
        componentModel.file = data['file']        
        componentModel.transactionId = data['idTransaksi']

        return componentModel


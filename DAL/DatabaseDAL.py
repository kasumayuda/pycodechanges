from Helper.Configuration import Configuration
from Helper.Logger import Logger
#import mysql.connector
import pymysql.cursors
import json

class DatabaseDAL:
    
    def __init__(self):
        try:            
            self._connect(Configuration.ReadConfig("Database"))
        except:
            pass

    def _connect(self, dbConfig):
        # self.dbConnect = mysql.connector.connect(
        #         host= dbConfig['Server'],
        #         user= dbConfig['User'],
        #         passwd= dbConfig['Password'],
        #         database = dbConfig['Database'],
        #         auth_plugin= dbConfig['AuthPlugin']
        #         )
        self.dbConnect = pymysql.connect(
                host= dbConfig['Server'],
                user= dbConfig['User'],
                password= dbConfig['Password'],
                db = dbConfig['Database'],
                charset='utf8mb4',
                cursorclass=pymysql.cursors.DictCursor)
        # self.dbConnect = pymysql.connect(host='localhost',
        #                      user='user',
        #                      password='passwd',
        #                      db='db',
        #                      charset='utf8mb4',
        #                      cursorclass=pymysql.cursors.DictCursor)

        self.dbCursor = self.dbConnect.cursor()

    def execute(self, sqlString, sqlValue):
        try:
            
            sqlScript = 'sqlString: ' + sqlString
            sqlScript += '\n sqlValue: ' + str(sqlValue)
            Logger.LogInfo(sqlScript)

            self.dbCursor.execute(sqlString, sqlValue)
            self.dbConnect.commit()
            return self.dbCursor.lastrowid
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0
        
    def fetch(self, sqlString):
        try:
            #Logger.LogInfo('Fetch: ' + sqlString)

            self.dbCursor.execute(sqlString)
            return self.dbCursor.fetchall()
        except Exception as ex:
            Logger.LogError(str(ex))
            return None
    
    
import pandas as panda
from Helper.Logger import Logger
from DataModel.TransactionModel import TransactionModel
from DataModel.ComponentModel import ComponentModel

class CSVTransactionDAL:

    def __init__(self):
        pass

    def GetAll(self, csvFile):
        try:
            data = panda.read_csv(csvFile)
            return self._mapDataToModels(data)
        except Exception as ex:
            Logger.LogError(str(ex))

    """
        map transaction to model
    """
    def _mapDataToModels(self, dataList):
        if (dataList is None): return None
        transactionModels = []
        dataList.drop_duplicates(subset="transactionId", keep=False, inplace=False)
        for transaction in dataList.transactionId.drop_duplicates():
            transactionModel = TransactionModel()
            transactionModel.transactionId = transaction
            transactionModel.components = self._mapComponentDataToModels(dataList.loc[dataList['transactionId'] == transaction])
            transactionModels.append(transactionModel)
            
        return transactionModels
                

    """
        map component to model
    """
    def _mapComponentDataToModels(self, componentDataList):
        if (componentDataList is None): return TransactionModel()

        components = []
        for x in range(0, len(componentDataList)):
            componentModel = ComponentModel()
            componentModel.transactionId = componentDataList.iloc[x]['transactionId']
            componentModel.method = componentDataList.iloc[x]['method']
            componentModel.entity = componentDataList.iloc[x]['class']
            componentModel.file = componentDataList.iloc[x]['file']
            components.append(componentModel)

        return components
from DataModel.CategoryModel import CategoryModel
from .DatabaseDAL import DatabaseDAL
from Helper.Logger import Logger

class CategoryDAL:
    _tableName = "kategori"

    def __init__(self):
        self.dbDAL = DatabaseDAL()

    def GetById(self, id):
        sql = "SELECT * FROM "+ self._tableName +" WHERE kategoriId =" + id
        return self._mapDataToModel(self.dbDAL.fetch(sql))

    def GetAll(self):
        sql = "SELECT * FROM "+ self._tableName
        categories = self.dbDAL.fetch(sql)

        categoryModels = []
        for category in categories: categoryModels.append(self._mapDataToModel(category))

        return categoryModels

    def _mapDataToModel(self, data):
        categoryModel = CategoryModel()
        
        if (data == None): return categoryModel

        categoryModel.categoryId = data[0]
        categoryModel.category = data[1]
        categoryModel.order = data[2]

        return categoryModel
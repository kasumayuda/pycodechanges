from DataModel.TransactionModel import TransactionModel
from .DatabaseDAL import DatabaseDAL
from .ComponentDAL import ComponentDAL
from Helper.Logger import Logger

class TransactionDAL:
    _tableName = "transaksi"
    
    def __init__(self):
        self.dbDAL = DatabaseDAL()
                
    def Insert(self, transactionModel):
        try:
            sql = "INSERT INTO " + self._tableName + " (tanggal) VALUES (%s)"
            val = (transactionModel.date,)
            transactionModel.transactionId = self.dbDAL.execute(sql, val)            
            return transactionModel
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0
    
    def Update(self, transactionModel):
        try:
            sql = "UPDATE " + self._tableName + " SET tanggal=%s WHERE idTransaksi=%s"
            val = (transactionModel.date, transactionModel.transactionId)
            transactionModel.transactionId = self.dbDAL.execute(sql, val)            
            return transactionModel
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0

    def GetAll(self):
        try:
            sql = "SELECT * FROM "+ self._tableName + " ORDER BY tanggal"
            return self._mapDataToModelsWithComponents(self.dbDAL.fetch(sql))
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0

    def GetAllNoComponent(self):
        try:
            sql = "SELECT * FROM "+ self._tableName
            return self._mapDataToModels(self.dbDAL.fetch(sql))
        except Exception as ex:
            Logger.LogError(str(ex))
            return 0

    """
        map the returned list of transaction to model list
    """
    def _mapDataToModels(self, dataList):
        if (dataList == None): return None
        transactionModels = []
        for data in dataList:
            transactionModels.append(self._mapDataToModel(data))
        return transactionModels

    def _mapDataToModelsWithComponents(self, dataList):
        if (dataList == None): return None
        transactionModels = []
        
        componentDAL = ComponentDAL()
        allComponents = componentDAL.GetAll()
        for data in dataList:
            components = [item for item in allComponents if item.transactionId == data['idTransaksi']]
            transactionModels.append(self._mapDataToModelTransactionComponent(data, components))
        return transactionModels

    """
        map a transaction to a transaction data model
    """
    def _mapDataToModel(self, data):
        transactionModel = TransactionModel()
        
        if (data == None): return None

        transactionModel.transactionId = data['idTransaksi']
        transactionModel.date = data['tanggal']

        return transactionModel

    def _mapDataToModelTransactionComponent(self, data, allComponents):        
        """
            map a transaction to a transaction data model
            plus get its components
        """
        if (data == None): return TransactionModel()

        transactionModel = self._mapDataToModel(data)
        # components = []
        # for component in allComponents:
        #     if (component.transactionId != transactionModel.transactionId): continue
        #     components.append(component)       
        # transactionModel.components = components
        #transactionModel.components = [item for item in allComponents if item.transactionId == transactionModel.transactionId] #componentDAL.GetByTransactionId(transactionModel.transactionId)
        transactionModel.components = allComponents

        return transactionModel
    
    """
        map a transaction to a transaction data model
        plus get its components
    """
    def _mapDataToModelTransactionComponentNoComponent(self, data):        
        if (data == None): return TransactionModel()

        transactionModel = self._mapDataToModel(data)
        
        return transactionModel

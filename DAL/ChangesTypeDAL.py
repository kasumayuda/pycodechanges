from DataModel.ChangesTypeModel import ChangeTypeModel
from .DatabaseDAL import DatabaseDAL
from Helper.Logger import Logger

class ChangesTypeDAL:
    _tableName = "jenisperubahan"

    def __init__(self):
        self.dbDAL = DatabaseDAL()

    def GetById(self, id):
        sql = "SELECT * FROM "+ self._tableName +" WHERE idJenisPerubahan =" + id
        return self._mapDataToModel(self.dbDAL.fetch(sql))

    def GetAll(self):
        sql = "SELECT * FROM "+ self._tableName
        changesTypes = self.dbDAL.fetch(sql)

        changesTypeModels = []
        for changesType in changesTypes: changesTypeModels.append(self._mapDataToModel(changesType))

        return changesTypeModels

    def _mapDataToModel(self, data):
        changeTypeModel = ChangeTypeModel()
        
        if (data == None): return changeTypeModel

        changeTypeModel.changeTypeId = data[0]
        changeTypeModel.change = data[1]

        return changeTypeModel
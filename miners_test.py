from Miners.Miners import Miners
from DataModel.RuleModel import RuleModel
from Helper.Configuration import Configuration
from DataModel.ComponentModel import ComponentModel
from DataModel.TransactionModel import TransactionModel
from DAL.TransactionDAL import TransactionDAL
from DAL.ComponentDAL import ComponentDAL
import datetime

def _SetupQuery():
    queryA = ComponentModel()
    queryA.method = "Method A_2"
    queryA.entity = "Class A"
    queryA.file = "File A"

    queryB = ComponentModel()
    queryB.method = "Method A_1"
    queryB.entity = "Class A"
    queryB.file = "File A"

    queries = [queryA, queryB]
    return queries

def _SetupTransactions():
    componentsTransA = ComponentModel()
    componentsTransA.componentId = 1
    componentsTransA.transactionId = 1
    componentsTransA.method = "Method A_1"
    componentsTransA.entity = "Class A"
    componentsTransA.file = "File A"

    componentsTransB = ComponentModel()
    componentsTransB.componentId = 2
    componentsTransB.transactionId = 1
    componentsTransB.method = "Method A_2"
    componentsTransB.entity = "Class A"
    componentsTransB.file = "File A"

    componentsTransC = ComponentModel()
    componentsTransC.componentId = 3
    componentsTransC.transactionId = 1
    componentsTransC.method = "Method A_3"
    componentsTransC.entity = "Class A"
    componentsTransC.file = "File A"

    transModelA = TransactionModel()
    transModelA.transactionId = 1
    transModelA.date = '2019-04-02'
    transModelA.components = [componentsTransA, componentsTransB, componentsTransC]

    componentsTransD = ComponentModel()
    componentsTransD.componentId = 4
    componentsTransD.transactionId = 2
    componentsTransD.method = "Method A_1"
    componentsTransD.entity = "Class A"
    componentsTransD.file = "File A"

    componentsTransF = ComponentModel()
    componentsTransF.componentId = 5
    componentsTransF.transactionId = 2
    componentsTransF.method = "Method A_2"
    componentsTransF.entity = "Class A"
    componentsTransF.file = "File A"

    componentsTransE = ComponentModel()
    componentsTransE.componentId = 6
    componentsTransE.transactionId = 2
    componentsTransE.method = "Method A_X"
    componentsTransE.entity = "Class A"
    componentsTransE.file = "File A"

    transModelB = TransactionModel()
    transModelB.transactionId = 2
    transModelB.date = '2019-04-03'
    transModelB.components = [componentsTransD, componentsTransE, componentsTransF]

    componentsTransI = ComponentModel()
    componentsTransI.componentId = 7
    componentsTransI.transactionId = 2
    componentsTransI.method = "Method A_1"
    componentsTransI.entity = "Class A"
    componentsTransI.file = "File A"

    componentsTransG = ComponentModel()
    componentsTransG.componentId = 8
    componentsTransG.transactionId = 3
    componentsTransG.method = "Method A_3"
    componentsTransG.entity = "Class A"
    componentsTransG.file = "File A"

    componentsTransH = ComponentModel()
    componentsTransH.componentId = 9
    componentsTransH.transactionId = 3
    componentsTransH.method = "Method A_X"
    componentsTransH.entity = "Class A"
    componentsTransH.file = "File A"

    transModelC = TransactionModel()
    transModelC.transactionId = 3
    transModelC.date = '2019-04-04'
    transModelC.components = []
    transModelC.components.append(componentsTransI)
    transModelC.components.append(componentsTransG)
    transModelC.components.append(componentsTransH)

    transactions  = [transModelA, transModelB, transModelC]
    return transactions

def _SetupComponentInitDefault():
        component = ComponentModel()
        component.method = "initDefault"
        component.entity = "Comp"
        component.file = "Comp.java"
        return component

def _SetupComponentCalculate():
        component = ComponentModel()
        component.method = "calculate"
        component.entity = "Foo"
        component.file = "Bar.Java"
        return component

def _SetupComponentCalculateTax():
        component = ComponentModel()
        component.method = "calculateTax"
        component.entity = "Comp"
        component.file = "Comp.java"
        return component

def _SetupComponentDisplay():
        component = ComponentModel()
        component.method = "display"
        component.entity = "Comp"
        component.file = "Comp.java"
        return component

def _SetupComponentReadSalary():
        component = ComponentModel()
        component.method = "readSalary"
        component.entity = "Employee"
        component.file = "Employee.java"
        return component

def _SetupTransactionA():
        componentsTransA = _SetupComponentInitDefault()
        componentsTransA.componentId = 1
        componentsTransA.transactionId = 1

        componentsTransB = _SetupComponentCalculate()
        componentsTransB.componentId = 2
        componentsTransB.transactionId = 1

        componentsTransC = _SetupComponentCalculateTax()
        componentsTransC.componentId = 3
        componentsTransC.transactionId = 1

        transModelA = TransactionModel()
        transModelA.transactionId = 1
        transModelA.date = '2019-04-02'
        transModelA.components = []
        transModelA.components.append(componentsTransA)
        transModelA.components.append(componentsTransB)
        transModelA.components.append(componentsTransC)

        return transModelA

def _SetupTransactionB():
        componentsTransD = _SetupComponentCalculate()
        componentsTransD.componentId = 4
        componentsTransD.transactionId = 2

        componentsTransE = _SetupComponentCalculateTax()
        componentsTransE.componentId = 5
        componentsTransE.transactionId = 2

        transModelB = TransactionModel()
        transModelB.transactionId = 2
        transModelB.date = '2019-04-02'
        transModelB.components = []
        transModelB.components.append(componentsTransD)
        transModelB.components.append(componentsTransE)

        return transModelB

def _SetupTransactionC():
        componentsTransD = _SetupComponentDisplay()
        componentsTransD.componentId = 6
        componentsTransD.transactionId = 3

        componentsTransE = _SetupComponentReadSalary()
        componentsTransE.componentId = 7
        componentsTransE.transactionId = 3

        transaction = TransactionModel()
        transaction.transactionId = 3
        transaction.date = '2019-04-02'
        transaction.components = []
        transaction.components.append(componentsTransD)
        transaction.components.append(componentsTransE)

        return transaction

def _SetupTransactionD():
        transactionId = 4
        componentsTransA = _SetupComponentInitDefault()
        componentsTransA.componentId = 8
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculate()
        componentsTransB.componentId = 9
        componentsTransB.transactionId = transactionId

        componentsTransC = _SetupComponentCalculateTax()
        componentsTransC.componentId = 10
        componentsTransC.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-02'

        transaction.components = []
        transaction.components.append(componentsTransA) 
        transaction.components.append(componentsTransB) 
        transaction.components.append(componentsTransC)

        return transaction

def _SetupTransactionE():
        transactionId = 5
        componentsTransA = _SetupComponentCalculateTax()
        componentsTransA.componentId = 11
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculate()
        componentsTransB.componentId = 12
        componentsTransB.transactionId = transactionId

        componentsTransC = _SetupComponentDisplay()
        componentsTransC.componentId = 13
        componentsTransC.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-02'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)
        transaction.components.append(componentsTransC)

        return transaction

def _SetupTransactionF():
        transactionId = 6
        componentsTransA = _SetupComponentInitDefault()
        componentsTransA.componentId = 14
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculateTax()
        componentsTransB.componentId = 15
        componentsTransB.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-02'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)

        return transaction

def _SetupTransactionG():
        transactionId = 7
        componentsTransA = _SetupComponentCalculate()
        componentsTransA.componentId = 16
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculateTax()
        componentsTransB.componentId = 17
        componentsTransB.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-02'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)        

        return transaction

def _SetupTransactionH():
        transactionId = 8
        componentsTransA = _SetupComponentCalculate()
        componentsTransA.componentId = 18
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculateTax()
        componentsTransB.componentId = 19
        componentsTransB.transactionId = transactionId

        componentsTransC = _SetupComponentInitDefault()
        componentsTransC.componentId = 20
        componentsTransC.transactionId = transactionId

        componentsTransD = _SetupComponentDisplay()
        componentsTransD.componentId = 21
        componentsTransD.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-12'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)
        transaction.components.append(componentsTransC)
        transaction.components.append(componentsTransD)        

        return transaction

def _SetupTransactionI():
        transactionId = 9
        componentsTransA = _SetupComponentCalculate()
        componentsTransA.componentId = 22
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculateTax()
        componentsTransB.componentId = 23
        componentsTransB.transactionId = transactionId

        componentsTransC = _SetupComponentInitDefault()
        componentsTransC.componentId = 24
        componentsTransC.transactionId = transactionId

        componentsTransD = _SetupComponentDisplay()
        componentsTransD.componentId = 25
        componentsTransD.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-13'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)
        transaction.components.append(componentsTransC)
        transaction.components.append(componentsTransD)        

        return transaction
        
def _SetupTransactionJ():
        transactionId = 10
        componentsTransA = _SetupComponentCalculate()
        componentsTransA.componentId = 26
        componentsTransA.transactionId = transactionId

        componentsTransB = _SetupComponentCalculateTax()
        componentsTransB.componentId = 27
        componentsTransB.transactionId = transactionId

        componentsTransC = _SetupComponentReadSalary()
        componentsTransC.componentId = 28
        componentsTransC.transactionId = transactionId

        componentsTransD = _SetupComponentDisplay()
        componentsTransD.componentId = 29
        componentsTransD.transactionId = transactionId

        transaction = TransactionModel()
        transaction.transactionId = transactionId
        transaction.date = '2019-04-14'
        transaction.components = []
        transaction.components.append(componentsTransA)
        transaction.components.append(componentsTransB)
        transaction.components.append(componentsTransC)
        transaction.components.append(componentsTransD)        

        return transaction

def _SetupTransactionsTest():
        return [_SetupTransactionA(), _SetupTransactionB(), _SetupTransactionC(),
        _SetupTransactionD(), _SetupTransactionE(), _SetupTransactionF(),
         _SetupTransactionG(), _SetupTransactionH(), _SetupTransactionI(), _SetupTransactionJ()]

def _SetupQuerySecondTest():
        componentA = ComponentModel()
        componentA.method = "calculate"
        componentA.entity = "Foo"
        componentA.file = "Bar.Java"

        componentB = ComponentModel()
        componentB.method = "calculateTax"
        componentB.entity = "Comp"
        componentB.file = "Comp.java"

        componentC = ComponentModel()
        componentC.method = "initDefault"
        componentC.entity = "Comp"
        componentC.file = "Comp.java"

        componentD = ComponentModel()
        componentD.method = "readSalary"
        componentD.entity = "Employee"
        componentD.file = "Employee.java"

        queries = [componentB, componentA,componentC, componentD]
        return queries

def evaluate_test():
    miners = Miners()

    rules = []
    ruleModelA = RuleModel(["x"],["c"])
    rules.append(ruleModelA)

    ruleModelC = RuleModel(["x"],["g"])
    rules.append(ruleModelC)

    ruleModelF = RuleModel(["x"],["f"])
    rules.append(ruleModelF)

    ruleModelG = RuleModel(["x"],["a"])
    rules.append(ruleModelG)

    ruleModelD = RuleModel(["x"],["d"])
    rules.append(ruleModelD)

    expectedOutcome = ["c","d","f"]

    print(miners.evaluate(rules, expectedOutcome))

def filteredHistories_test():
    miners = Miners()

    filteredHistories = miners.getFilteredHistories(_SetupTransactions(), _SetupQuery())
    print(len(filteredHistories))
    for filteredHistory in filteredHistories:
        print("transaction " +str(filteredHistory.transactionId))
        for component in filteredHistory.components:
            print(component.method + " " + component.entity + " " + component.file)

def createRule_test():
    miners = Miners()
    query = _SetupQuerySecondTest()
    filteredHistories = miners.getFilteredHistories(_SetupTransactionsTest(), query)    
    rules = miners.createRule(filteredHistories, query)
    for rule in rules:
        print(rule.name)
        print("support " + str(rule.support))
        print("confidence " + str(rule.confidence))
        print("=====================================")
    
def execute_Test():
        miners = Miners()
        miners.execute()

def sortRule_test():
    miners = Miners()
    query = _SetupQuerySecondTest()
    filteredHistories = miners.getFilteredHistories(_SetupTransactionsTest(), query)
    rules = miners.createRule(filteredHistories, query)
    sortedRules = miners.sortRules(rules)
    for rule in sortedRules:
        print(rule.name)
        print("support " + str(rule.support))
        print("confidence " + str(rule.confidence))
        print("=====================================")

def getAllTransactions_test():
        timeStart = datetime.datetime.now()

        Configuration.ConfigFile = "config.hangfire.json"

        transactionDAL = TransactionDAL()
        allTransactions = transactionDAL.GetAll()
        #allTransactions = transactionDAL.GetAllNoComponent()
        
        timeFinish = datetime.datetime.now()
        timeDiff = timeFinish - timeStart
        print("%d data in %d seconds" % (len(allTransactions),timeDiff.seconds))
        # dataLength = len(allTransactions)
        # transactionLimit = round((70 / 100) * dataLength)
        # print('limit : %d' % (transactionLimit))
        # transactions = allTransactions[:transactionLimit]
        # queryCandidates = set(allTransactions) - set(transactions)

        # print("all transactions : %d" % len(allTransactions))
        # print("transactions : %d" % len(transactions))
        # print("query : %d" % len(queryCandidates))

        # queryCandidatesFinals = []
        # for queryLimit in range(2,12):
        #         counter = 0
        #         for queryCandidate in list(queryCandidates):
        #                 if (counter == 4): break

        #                 componentLength = len(queryCandidate.components)
        #                 if (componentLength != queryLimit): continue

        #                 queryCandidatesFinals.append(queryCandidate)
        #                 counter += 1

        #         print("%d : %d" % (queryLimit, counter))


        # componentCounter = 0
        # for trans in transactions:
        #         componentCounter += len(trans.components)

        # print("componentCounter %d" % componentCounter)
        
        # qLen = len(queryCandidatesFinals)
        # counter = 0
        # for index in range(1,(qLen)):
        #     if (counter == 5): break
        #     query = list(queryCandidatesFinals)[:index]
        #     print(query)
        #     expectedOutcome = list(queryCandidatesFinals)[index:]
        #     print(expectedOutcome)
        #     counter += 1

        # timeFinish = datetime.datetime.now()
        # timeDiff = timeFinish - timeStart
        # print("%d seconds" % timeDiff.seconds)

def getAllComponents_Test():
        timeStart = datetime.datetime.now()

        Configuration.ConfigFile = "config.hangfire.json"

        componentDAL = ComponentDAL()
        allComponents = componentDAL.GetAll()
        
        timeFinish = datetime.datetime.now()
        timeDiff = timeFinish - timeStart
        print("%d data in %d seconds" % (len(allComponents),timeDiff.seconds))

def createQuery_Test():
        transactionDAL = TransactionDAL()
        allTransactions = transactionDAL.GetAll()
        history = allTransactions[:round((70 / 100) * len(allTransactions))]
        queryCandidates = set(allTransactions) - set(history)
        queryCandidates = sorted(queryCandidates, key = lambda x: x.transactionId)        
        allQueryCandidatesFinals = {}
        for queryLimit in range(2,9):
                components = []
                for queryCandidate in list(queryCandidates):
                        if (len(queryCandidate.components) == queryLimit and len(components) < 4): 
                                components.append(queryCandidate.components)

                allQueryCandidatesFinals[queryLimit] = components

        for queryLimit in range(2,9):
                for components in allQueryCandidatesFinals[queryLimit]:
                        for component in components:                                
                                print(component.componentId)
        
#filteredHistories_test()
createRule_test()
#sortRule_test()
#evaluate_test()
#getAllTransactions_test()
#getAllComponents_Test()
#execute_Test()
#createQuery_Test()